import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFieldNavigator extends TextInputFormatter {
  final FocusNode focusNodeNext;
  final FocusNode focusNodePrev;
  final BuildContext context;

  TextFieldNavigator({
    @required this.context,
    @required this.focusNodeNext,
    @required this.focusNodePrev,
  }) {
    assert(context != null);

    assert(focusNodeNext != null);
    assert(focusNodePrev != null);
  }

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (oldValue.text.length == 3 && newValue.text.length == 4) {
      FocusScope.of(context).requestFocus(focusNodeNext);
    } else if (oldValue.text.length == 1 && newValue.text.length == 0) {
      FocusScope.of(context).requestFocus(focusNodePrev);
    }
    return newValue;
  }
}

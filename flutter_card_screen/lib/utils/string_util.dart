class StringUtil {

  static String REMOVE_CARD = "Remove Card";
  static String CARD_NUMBER = "Card Number";
  static String NUMBER = "xxxx";
  static String PLEASE_INPUT_NUMBER_ONLY = "Please input numbers only";
  static String PASSWORD = "Password";
  static String MANAGE_CARDS = "Manage Cards";
  static String DOB = "Date of Birth";
  static String DOB_FORMAT = "YY MM DD";
  static String EXPIRY_DATE = "Expiry date";
  static String EXPIRY_DATE_FORMAT = "MM / YY";
  static String PLEASE_ENTER_EXPIRE_DATE = "Please enter expire date";
  static String PLEASE_ENTER_DOB = "Please enter expire DOB";

}

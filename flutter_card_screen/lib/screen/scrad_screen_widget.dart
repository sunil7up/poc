import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttercardscreen/carousel/carousel_options.dart';
import 'package:fluttercardscreen/carousel/carousel_slider.dart';
import 'package:fluttercardscreen/utils/TextFieldNavigator.dart';
import 'package:fluttercardscreen/utils/card_formatter.dart';
import 'package:fluttercardscreen/utils/colors_constant.dart';
import 'package:fluttercardscreen/utils/image_url_constant.dart';
import 'package:fluttercardscreen/utils/string_util.dart';
import 'package:fluttercardscreen/widgets/custom_text.dart';

class CardScreenWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: const Color(0xFFFFFFFF),
        primaryColorDark: const Color(0xFF167F67),
        accentColor: const Color(0xFF167F67),
      ),
      home: CardScreenPage(title: 'Flutter Demo Home Page'),
    );
  }
}

class CardScreenPage extends StatefulWidget {
  CardScreenPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _CardScreenPageState createState() => _CardScreenPageState();
}

class _CardScreenPageState extends State<CardScreenPage> {
  final _formKey = GlobalKey<FormState>();

  final _teOtpDigitOne = TextEditingController();
  final _teOtpDigitTwo = TextEditingController();
  final _teOtpDigitThree = TextEditingController();
  final _teOtpDigitFour = TextEditingController();

  FocusNode _focusNodeDigitOne = new FocusNode();
  FocusNode _focusNodeDigitTwo = new FocusNode();
  FocusNode _focusNodeDigitThree = new FocusNode();
  FocusNode _focusNodeDigitFour = new FocusNode();

  final _tePassword = TextEditingController();
  FocusNode _focusNodePassword = FocusNode();

  final _teDob = TextEditingController();
  FocusNode _focusNodeDob = FocusNode();

  final _teExpireDate = TextEditingController();
  FocusNode _focusNodeExpireDate = FocusNode();

  List<Widget> imageSliders;

  @override
  void initState() {
    super.initState();
    createCardWidgets();
  }

  void _submit() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      FocusScope.of(context).requestFocus(FocusNode());
    }
  }

  @override
  Widget build(BuildContext context) {
    var loginForm = Form(
        key: _formKey,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: CustomText(
                  label: StringUtil.CARD_NUMBER,
                  fontSize: 13.0,
                  fontName: "pp_medium",
                  textAlign: TextAlign.right,
                  textColor: ColorConstants.BLACK,
                ).text(),
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: inputFiledNew(
                        _teOtpDigitOne,
                        _focusNodeDigitOne,
                        "",
                        TextInputType.number,
                        StringUtil.NUMBER,
                        [
                          TextFieldNavigator(context: context,
                              focusNodeNext: _focusNodeDigitTwo,
                              focusNodePrev: _focusNodeDigitOne),
                          LengthLimitingTextInputFormatter(4)
                        ],
                        false,
                        TextAlign.center),
                    flex: 1,
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Expanded(
                    child: inputFiledNew(
                        _teOtpDigitTwo,
                        _focusNodeDigitTwo,
                        "",
                        TextInputType.number,
                        StringUtil.NUMBER,
                        [
                          TextFieldNavigator( context: context,
                              focusNodeNext:_focusNodeDigitThree,
                              focusNodePrev:_focusNodeDigitOne),
                          LengthLimitingTextInputFormatter(4)
                        ],
                        false,
                        TextAlign.center),
                    flex: 1,
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Expanded(
                    child: inputFiledNew(
                        _teOtpDigitThree,
                        _focusNodeDigitThree,
                        "",
                        TextInputType.number,
                        StringUtil.NUMBER,
                        [
                          TextFieldNavigator( context: context,
                              focusNodeNext:_focusNodeDigitFour,
                              focusNodePrev:_focusNodeDigitTwo),
                          LengthLimitingTextInputFormatter(4)
                        ],
                        false,
                        TextAlign.center),
                    flex: 1,
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Expanded(
                    child: inputFiledNew(
                        _teOtpDigitFour,
                        _focusNodeDigitFour,
                       "",
                        TextInputType.number,
                        StringUtil.NUMBER,
                        [
                          TextFieldNavigator( context: context,
                              focusNodeNext:_focusNodeDigitFour,
                              focusNodePrev:_focusNodeDigitThree),
                          LengthLimitingTextInputFormatter(4)
                        ],
                        false,
                        TextAlign.center),
                    flex: 1,
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0, top: 15.0),
                child: CustomText(
                  label: StringUtil.PASSWORD,
                  fontSize: 13.0,
                  fontName: "pp_medium",
                  textAlign: TextAlign.right,
                  textColor: ColorConstants.BLACK,
                ).text(),
              ),
              inputFiledNew(
                  _tePassword,
                  _focusNodePassword,
                  StringUtil.PLEASE_INPUT_NUMBER_ONLY,
                  TextInputType.visiblePassword,
                  StringUtil.PASSWORD,
                  [LengthLimitingTextInputFormatter(50)],
                  true,
                  TextAlign.left),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 15.0),
                            child: CustomText(
                              label: StringUtil.DOB,
                              fontSize: 13.0,
                              fontName: "pp_medium",
                              textAlign: TextAlign.right,
                              textColor: ColorConstants.BLACK,
                            ).text(),
                          ),
                          inputFiledNew(
                              _teDob,
                              _focusNodeDob,
                              StringUtil.PLEASE_ENTER_DOB,
                              TextInputType.number,
                              StringUtil.DOB_FORMAT,
                              [
                                CardFormatter(
                                  mask: 'xx.xx.xx',
                                  separator: '.',
                                ),
                              ],
                              false,
                              TextAlign.center),
                        ],
                      ),
                      flex: 1,
                    ),
                    SizedBox(
                      width: 20.0,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 15.0),
                            child: CustomText(
                              label: StringUtil.EXPIRY_DATE,
                              fontSize: 13.0,
                              fontName: "pp_medium",
                              textAlign: TextAlign.right,
                              textColor: ColorConstants.BLACK,
                            ).text(),
                          ),
                          inputFiledNew(
                              _teExpireDate,
                              _focusNodeExpireDate,
                              StringUtil.PLEASE_ENTER_EXPIRE_DATE,
                              TextInputType.number,
                              StringUtil.EXPIRY_DATE_FORMAT,
                              [
                                CardFormatter(
                                  mask: 'xx/xx',
                                  separator: '/',
                                ),
                              ],
                              false,
                              TextAlign.center),
                        ],
                      ),
                      flex: 1,
                    )
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  _submit();
                },
                child: Container(
                  margin: EdgeInsets.fromLTRB(40.0, 40.0, 40.0, 0.0),
                  child: buttonMedium(
                      StringUtil.REMOVE_CARD,
                      EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                      Color(ColorConstants.COLOR_BUTTON),
                      Color(0xFFFFFFFF),
                      16.0),
                ),
              ),
            ]));

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios,color:  Color(ColorConstants.TITILE),),
          onPressed: () {},
        ),
        title: CustomText(
          label: StringUtil.MANAGE_CARDS,
          fontSize: 20.0,
          fontName: "pp_medium",
          textAlign: TextAlign.center,
          textColor: ColorConstants.TITILE,
        ).text(),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 25.0),
              child: CarouselSlider(
                options: CarouselOptions(
                  autoPlay: true,
                  aspectRatio: 2.0,
                  enlargeCenterPage: true,
                ),
                items: imageSliders,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 50.0, right: 50.0, top: 30.0, bottom: 50.0),
              child: loginForm,
            ),
          ],
        ),
      ),
    );
  }

  void createCardWidgets() {
    List<String> imgList = [
      SvgUtil.CARD_BLUE,
      SvgUtil.CARD_BLUE,
      SvgUtil.CARD_BLUE,
    ];
    imageSliders = imgList
        .map((item) => Container(
              child: Container(
                child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    child: Stack(
                      children: <Widget>[
                        Image.asset(item, fit: BoxFit.cover, width: 1500.0),
                      ],
                    )),
              ),
            ))
        .toList();
  }
}

Widget inputFiledNew(
    TextEditingController teController,
    FocusNode focusNode,
    String errorMsg,
    TextInputType inputType,
    String hintMsg,
    List<TextInputFormatter> cardFormatter,
    bool isHide,
    TextAlign textAlign) {
  return TextFormField(
    validator: (val) => val.isEmpty ? errorMsg : null,
    onSaved: (val) => val,
    controller: teController,
    keyboardType: inputType,
    focusNode: focusNode,
    obscureText: isHide,

    textAlign: textAlign,
    style: TextStyle(color: Color(ColorConstants.TITILE),fontFamily: "pp_medium",),
    inputFormatters: cardFormatter,
    decoration: InputDecoration(
      hintText: hintMsg,
      hintStyle: TextStyle(fontSize: 14),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
        borderSide: BorderSide(
          width: 0,
          style: BorderStyle.none,
        ),
      ),
      filled: true,
      contentPadding: EdgeInsets.all(10),
      fillColor: Color(ColorConstants.COLOR_INPUT_FIELD),
    ),
  );
}

Widget buttonMedium(String buttonLabel, EdgeInsets margin, Color bgColor,
    Color textColor, double textSize) {
  var loginBtn = Container(
    margin: margin,
    padding: EdgeInsets.all(30.0),
    alignment: FractionalOffset.center,
    decoration: BoxDecoration(
      color: bgColor,
      borderRadius: BorderRadius.all(const Radius.circular(60.0)),
    ),
    child: Text(
      buttonLabel,
      style: TextStyle(
          color: textColor, fontSize: textSize, fontWeight: FontWeight.bold),
    ),
  );
  return loginBtn;
}

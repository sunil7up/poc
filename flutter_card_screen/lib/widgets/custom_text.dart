import 'package:flutter/cupertino.dart';

class CustomText {
  final String label;
  final double fontSize;
  final String fontName;
  final int textColor;
  final TextAlign textAlign;
  final int maxLines;

  CustomText(
      {@required this.label,
      this.fontSize = 10.0,
      this.fontName,
      this.textColor = 0xFF000000,
      this.textAlign = TextAlign.start,
      this.maxLines=1});

  Widget text() {
    var text =  Text(
      label,
      textAlign: textAlign,
      overflow: TextOverflow.ellipsis,
      maxLines: maxLines,
      style:  TextStyle(
        color: Color(textColor),
        fontSize: fontSize,
        fontFamily: fontName,
      ),
    );
    return text;
  }
}
